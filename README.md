#Foot14

Foot14 projecto ipm usando o bootstrap do twitter.

* _Dimenções do ecran:_ 320 * 460
* _Barra baixo:_ 320 * 50
* _Barra titulo:_ 320 * 35
* _Central:_ 320 * 375

##Bibliotecas usadas:

* [Twitter Bootstrap](http://twitter.github.com/bootstrap/index.html)
* [Font Awesome](http://fortawesome.github.com/Font-Awesome/)
* [Inspiração](http://site.opcaojeans.com.br/maisopcao/wp-content/uploads/2010/10/adriana-lima02.jpg)
